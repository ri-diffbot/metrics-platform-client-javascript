# metrics-platform-client-javascript
Metrics client library for JavaScript, intended for use in MediaWiki.

See the [Metrics Platform](https://wikitech.wikimedia.org/wiki/Metrics_Platform) project page on Wikitech for details.

## Testing
Run tests with `npm test` and coverage with `npm coverage`.

